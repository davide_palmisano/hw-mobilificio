package hwMobili.DAO;

import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.tomcat.util.threads.ResizableExecutor;

import hwMobili.classi.Categoria;
import hwMobili.classi.Prodotto;
import hwMobili.connessione.ConnettoreDB;

public class CategoriaDAO implements DAO{

	@Override
	public ArrayList<Prodotto> getById(int id) throws SQLException {
		ArrayList<Prodotto> elenco = new ArrayList<Prodotto>();
   		Connection conn = ConnettoreDB.getIstanza().getConnessione();
        
       	String query = "SELECT categoria.nome, prodotto.nome, prodotto.descrizione, prodotto.codice, prodotto.prezzo  FROM prodotto\r\n"
       			+ "JOIN categoria_prodotto on prodotto.prodottoId = categoria_prodotto.prodotto_id\r\n"
       			+ "JOIN categoria on categoria_prodotto.categoria_id = categoria.categoriaId "
       			+ "WHERE categoria.categoriaId = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setInt(1, id);
       	ResultSet risultato = ps.executeQuery();
       	while(risultato.next()) {
       		Categoria cat_temp = new Categoria();
       		Prodotto prod_temp = new Prodotto();
       		cat_temp.setNome(risultato.getString(1));
       		prod_temp.setNome(risultato.getString(2));
       		prod_temp.setDescrizione(risultato.getString(3));
       		prod_temp.setCodice(risultato.getString(4));
       		prod_temp.setPrezzo(risultato.getFloat(5));
       		prod_temp.setElenco_cat(cat_temp);
       		elenco.add(prod_temp);
       	}
		return elenco;
	}

	@Override
	public ArrayList<Categoria> getAll() throws SQLException {
		ArrayList<Categoria> elenco = new ArrayList<Categoria>();
	       
   		Connection conn = ConnettoreDB.getIstanza().getConnessione();
           
       	String query = "SELECT categoriaId, nome, descrizione  FROM categoria";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	
       	ResultSet risultato = ps.executeQuery();
       	while(risultato.next()){
       		Categoria temp = new Categoria();
       		temp.setId(risultato.getInt(1));
       		temp.setNome(risultato.getString(2));
       		temp.setDescrizione(risultato.getString(3));
       		elenco.add(temp);
       	}
       	
       	return elenco;
	}

	@Override
	public void insert(Object t) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean delete(Object t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(Object t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

}
