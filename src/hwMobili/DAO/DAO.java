package hwMobili.DAO;
import java.sql.SQLException;
import java.util.ArrayList;

public interface DAO<T> {
	
	T getById(int id) throws SQLException;
	
	ArrayList<T> getAll() throws SQLException;
	
	void insert(T t) throws SQLException;
	
	boolean delete(T t) throws SQLException;
	
	boolean update(T t) throws SQLException;
}
