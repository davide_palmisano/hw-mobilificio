package hwMobili.classi;

import java.util.ArrayList;

public class Prodotto {
	private int id;
	private String nome;
	private String descrizione;
	private String codice;
	private float prezzo;
	private ArrayList<Categoria> elenco_cat = new ArrayList<Categoria>();
	
	public ArrayList<Categoria> getElenco_cat() {
		return elenco_cat;
	}
	public void setElenco_cat(Categoria cat) {
		this.elenco_cat.add(cat);
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	public float getPrezzo() {
		return prezzo;
	}
	public void setPrezzo(float prezzo) {
		this.prezzo = prezzo;
	}
	
	
}
