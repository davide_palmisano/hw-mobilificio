package hwMobili.classi;

import java.util.ArrayList;

public class Categoria {
	private int id ;
	private String nome;
	private String descrizione;
	private String codice;
	private ArrayList<Prodotto> elenco_cat;
	
	public ArrayList<Prodotto> getElenco_cat() {
		return elenco_cat;
	}
	public void setElenco_cat(ArrayList<Prodotto> elenco_cat) {
		this.elenco_cat = elenco_cat;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	
	

}
