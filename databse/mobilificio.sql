DROP DATABASE IF EXISTS hw_lez29;
CREATE DATABASE hw_lez29;
USE hw_lez29;

CREATE TABLE categoria(
categoriaId INTEGER NOT NULL AUTO_INCREMENT,
nome  VARCHAR(150) NOT NULL,
descrizione VARCHAR(250) NOT NULL,
codice  VARCHAR(10)  UNIQUE,
PRIMARY KEY (categoriaId)
);

CREATE TABLE prodotto(
prodottoId INTEGER NOT NULL AUTO_INCREMENT,
nome  VARCHAR(150) NOT NULL,
descrizione VARCHAR(250) NOT NULL,
codice  VARCHAR(10) UNIQUE,
prezzo float NOT NULL,
categoriaId INTEGER NOT NULL,
PRIMARY KEY (prodottoId)
);

CREATE TABLE categoria_prodotto(
categoria_id INTEGER NOT NULL,
prodotto_id INTEGER NOT NULL, 
PRIMARY KEY (categoria_id,prodotto_id),
foreign key (categoria_id) references categoria(categoriaId),
foreign key (prodotto_id) references prodotto(prodottoId)
);

CREATE TABLE Iscrizioni_Studente_Esame (
    studente_id INTEGER NOT NULL,
    esame_id INTEGER NOT NULL,
    data_iscrizione DATETIME NOT NULL,
    PRIMARY KEY (studente_id, esame_id),
    -- UNIQUE(studente_id, esame_id),
    FOREIGN KEY (studente_id) REFERENCES studente(StudenteID),
    FOREIGN KEY (esame_id) REFERENCES esame(EsameID)
);


INSERT INTO categoria (nome,descrizione) VALUES
("Mobili",""),
("Organizzare e contenere",""),
("Tessili",""),
("Articoli per la tavola e la cucina",""),
("Cucine ed elettrodomestici","");

INSERT INTO prodotto (nome,descrizione,prezzo) value
("Armadio","",120),
("Scaffale","",56),
("Posate","",4),
("Bicchieri","",3),
("Dispensa","",200),
("Cassettiere","",80) ; 


