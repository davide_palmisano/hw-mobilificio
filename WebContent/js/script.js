

function generaRiga(obj_categ) {
	let riga = "";
	for (let i = 0; i < obj_categ.length; i++) {
		riga += "<button class=\"dropdown-item\" onclick=\"stampaTabella(this)\" data-identificatore='"+obj_categ[i].id+"'>"+obj_categ[i].nome+"</button>";
		console.log(obj_categ[i].nome);
	}
	return riga;
}

function stampaTabella(obj_button){
	let id = $(obj_button).data("identificatore");
		$.ajax(
			{
				url: "http://localhost:8081/Mobilificio/prodottoByCategoria",
				method: "POST",
				data: {
					categoria_id : id 
				},
				success: function(elenco_prod){
					$("#table_prodByid").html(rigaTabella(elenco_prod));
					let var_categ = elenco_prod[0].elenco_cat[0].nome;
					$("#label-categoria").html("<h3>Categoria: "+var_categ+"</h3>");
				}
			}
		);
}

function rigaTabella(arrElencoProd){
	let riga = "";
	for(let i=0;i<arrElencoProd.length;i++){
		riga += "<tr>";
		riga += "<td>"+arrElencoProd[i].nome+"</td>";
		riga += "<td>"+arrElencoProd[i].descrizione+"</td>";
		riga += "<td>"+arrElencoProd[i].codice+"</td>";
		riga += "<td>"+arrElencoProd[i].prezzo+" $</td>";
		riga += "</tr>";
	}
	return riga ; 
}

function cercaCategorie(){
		$.ajax(
	{
		url: "http://localhost:8081/Mobilificio/cercacategorie",
		method: "POST",
		success: function(risultato) {
			let riga = generaRiga(risultato);
			$("#menu-categorie").html(riga);
		},
		error: function(ris_error){
			console.log("ERRORE");
			console.log(ris_error);
				}
	}
);
}
$(document).ready(
	function(){
		cercaCategorie();
		

	}
);