function stampaRiga(arr_obj_categoria){
	let riga = "";
	for(let i=0; i<arr_obj_categoria.length ; i++){
		let categoria = arr_obj_categoria[i];
		riga += "<tr>";
		riga += "<td>"+categoria.nome+"</td>";
		riga += "<td><input type=\"checkbox\" id=\"check-id\" value="+categoria.id+"></td>";	
		riga += "</tr>";	
		console.log(riga);
	}
	
	return riga ; 
}

$(document).ready(
	function(){
		$.ajax(
		{
			url: "http://localhost:8081/Mobilificio/cercacategorie",
			method: "POST",
			success: function(risultato) {
				let riga = stampaRiga(risultato);
				$("#tabella-categorie").html(riga);
			},
			error: function(ris_error){
				console.log("ERRORE");
				console.log(ris_error);
					}
		}
		);
		
		$("#btn-ins-prod").click(
			function(){
				$.ajax(
					{
						url : "http://localhost:8081/Mobilificio/aggiungiProdotto",
						method: "POST",
						data : {
							nome : $("#input_nome").val(),
							descrizione : $("#input_descrizione").val(),
							prezzo : $("#input_prezzo").val(),
							categoria : $("#check-id").attr('value'),
						}
					}
				);
			}
		);
	}
);